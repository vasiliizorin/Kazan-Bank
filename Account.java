public class Account {
	private int maxAmountOfTransactions = 100; // максимальное возможное число
												// транзакций
	public int lengthOfTransactions; // количество проведенных транзакций
	private String password; // пароль
	private int branch; // филиал
	// информация о владельце счета
	private String ownerName; // имя владельца
	private String ownerSurName; // фамилия владельца
	private String ownerBithday; // день рождения владельца
	private String ownerPassportNumber; // номер паспорта владельца
	private int type; // тип счета (1 - накопительный, 2 - чековый, 3 - бизнес)
	private double balance; // баланс
	private int id; // номер счета
	private boolean status; // текущий статус счета false = blocked

	// Транзакции данного аккаунта
	public Transaction[] transactions = new Transaction[maxAmountOfTransactions];

	// регистрация новой транзакции
	public boolean registrationTransaction(Transaction _newTransaction) {
		if (lengthOfTransactions < maxAmountOfTransactions) {
			transactions[lengthOfTransactions] = _newTransaction;
			lengthOfTransactions++;
			// регистрация проведенной транзакции в филиале
			Bank.branches[branch].registrationTransaction(_newTransaction);
			// регистрация транзакции в банке
			Bank.registrationTransaction(_newTransaction);
			return true;
		} else {
			return false;
		}
	}

	// получение количества проведенных транзакций
	public int getLengthOfTransactions() {
		return lengthOfTransactions;
	}

	// получение номера паспорта владельца счета
	public String getUserPassport() {
		return ownerPassportNumber;
	}

	// получение имени владельца счета
	public String getUserName() {
		return ownerName;
	}

	// получение фамилии владельца счета
	public String getUserSurname() {
		return ownerSurName;
	}

	// получение пароля
	public String getPassowrd() {
		return password;
	}

	public int getBranch() {
		return branch;
	}

	// получение текущего баланса
	public Double getBalance() {
		return balance;
	}

	// получение текущего баланса
	public void setBalance(double _balance) {
		if (status) {
			balance = _balance;
		}
	}

	// получение типа аккаунта
	public int getType() {
		return type;
	}

	// получить текущий статус аккаунта
	public boolean getStatus() {
		return status;
	}

	// конструктор объекта
	Account(String _name, String _surname, String _bithday, String _passportNumber, int newType, double startBalance,
			int _branch) {
		branch = _branch;
		ownerName = _name;
		ownerSurName = _surname;
		ownerBithday = _bithday;
		ownerPassportNumber = _passportNumber;
		type = newType;
		balance = startBalance;
		id = Bank.branches[_branch].getLengthAccounts();
		status = true;
	}

	// получение номера аккаунта
	public int getAccountID() {
		return id;
	}

	// процентная ставка
	public void interestPayment(double interest) {
		if (status) {
			balance = balance + (balance * interest) / 30;
			Transaction newTransaction = new Transaction(Transaction.Operations.INTEREST,
					Integer.toString(Bank.getDate()), Integer.toString(id + 1), "", true, (balance * interest) / 30, "",
					Integer.toString(branch + 1), "");
			registrationTransaction(newTransaction);
		}
	}

	// добавление средств на счет
	public boolean deposit(double amount) {
		if (status) {
			Transaction newTransaction;
			if (balance >= 0) {
				newTransaction = new Transaction(Transaction.Operations.DEPOSIT, Integer.toString(Bank.getDate()),
						Integer.toString(id + 1), "", true, amount, "", Integer.toString(branch + 1), "");
				if (registrationTransaction(newTransaction)) {
					balance += amount;
					// смена типа аккаунта при увеличении
					if (balance > 5000000 && type != 3) {
						type = 3;
						newTransaction = new Transaction(Transaction.Operations.TELLER,
								Integer.toString(Bank.getDate()), Integer.toString(id + 1), "", true, 0, "",
								Integer.toString(branch + 1), "");
						// проведение транзакции, если транзакци провести
						// нельзя, то
						// возвращается false
						registrationTransaction(newTransaction);
					}
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		return false;
	}

	// снятие средств со счета
	public boolean withdraw(double amount) {
		if (status) {
			if (amount > balance) {
				Transaction newTransaction = new Transaction(Transaction.Operations.WITHDRAW,
						Integer.toString(Bank.getDate()), Integer.toString(id + 1), "", false, amount, "",
						Integer.toString(branch + 1), "");
				registrationTransaction(newTransaction);
				return false;
			} else {
				Transaction newTransaction = new Transaction(Transaction.Operations.WITHDRAW,
						Integer.toString(Bank.getDate()), Integer.toString(id + 1), "", true, amount, "",
						Integer.toString(branch + 1), "");
				registrationTransaction(newTransaction);
				balance -= amount;
				return true;
			}
		}
		return false;
	}

	// блокировка аккаунта
	public boolean blockAccount() {
		Transaction newTransaction = new Transaction(Transaction.Operations.LOCKING, Integer.toString(Bank.getDate()),
				Integer.toString(id + 1), "", true, 0, "", Integer.toString(branch + 1), "");
		if (registrationTransaction(newTransaction)) {
			return true;
		} else {
			return false;
		}
	}

	// разблокировка аккаунта
	public boolean unlockAccount() {
		if (!status) {
			Transaction newTransaction = new Transaction(Transaction.Operations.UNLOCKING,
					Integer.toString(Bank.getDate()), Integer.toString(id + 1), "", true, 0, "",
					Integer.toString(branch + 1), "");
			if (registrationTransaction(newTransaction)) {
				status = true;
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	// смена типа аккаунта
	public boolean setTransactionType(int _type) {
		if (status) {
			Transaction newTransaction;
			boolean status = true;
			if (lengthOfTransactions < maxAmountOfTransactions) {
				switch (_type) {
				case 1: {
					if (balance >= 50000) {
						type = _type;
						status = true;
					} else {
						status = false;
					}
				}
				case 2: {
					if (balance >= 1000) {
						type = _type;
						status = true;
					} else {
						status = false;
					}
				}
				case 3: {
					if (balance >= 5000000) {
						type = _type;
						status = true;
					} else {
						status = false;
					}
				}
				default: {
					status = false;
				}
				}
				if (status) {
					newTransaction = new Transaction(Transaction.Operations.TELLER, Integer.toString(Bank.getDate()),
							Integer.toString(id + 1), "", true, 0, "", Integer.toString(branch + 1), "");
					registrationTransaction(newTransaction);
					return true;
				} else {
					newTransaction = new Transaction(Transaction.Operations.TELLER, Integer.toString(Bank.getDate()),
							Integer.toString(id + 1), "", false, 0, "", Integer.toString(branch + 1), "");
					registrationTransaction(newTransaction);
					return false;
				}
			} else {
				return false;
			}
		}
		return false;
	}

	// перевод средств
	public boolean transfer(int reciever, double amount) {
		if (status) {
			Transaction newTransaction;
			if (amount > Bank.branches[branch].accounts[id].getBalance() || amount <= 0) {
				// регистрация транзакции
				newTransaction = new Transaction(Transaction.Operations.TRANSFER, Integer.toString(Bank.getDate()),
						Integer.toString(id + 1), Integer.toString(reciever + 1), false, amount, "",
						Integer.toString(branch + 1), Integer.toString(reciever + 1));
				registrationTransaction(newTransaction);
				return false;
			} else {
				if (Bank.branches[branch].accounts[reciever].getStatus()
						&& Bank.branches[branch].accounts[reciever].lengthOfTransactions < 100) {
					Bank.branches[branch].accounts[id]
							.setBalance(Bank.branches[branch].accounts[id].getBalance() - amount);
					Bank.branches[branch].accounts[reciever]
							.setBalance(Bank.branches[branch].accounts[reciever].getBalance() + amount);
					// регистрация транзакции
					newTransaction = new Transaction(Transaction.Operations.TRANSFER, Integer.toString(Bank.getDate()),
							Integer.toString(id + 1), Integer.toString(reciever + 1), true, amount, "",
							Integer.toString(branch + 1), Integer.toString(reciever + 1));
					registrationTransaction(newTransaction);
					Bank.branches[branch].accounts[reciever].transactions[Bank.branches[branch].accounts[reciever].lengthOfTransactions] = newTransaction;
					return true;
				} else {
					// регистрация транзакции
					newTransaction = new Transaction(Transaction.Operations.TRANSFER, Integer.toString(Bank.getDate()),
							Integer.toString(id + 1), Integer.toString(reciever + 1), false, amount, "",
							Integer.toString(branch + 1), Integer.toString(reciever + 1));
					registrationTransaction(newTransaction);
					return false;
				}
			}
		}
		return false;
	}

}