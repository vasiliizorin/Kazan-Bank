public class Program {

	//печать отчета об операциях для всего банка
	public static void printBankReport() {
		System.out.println("Отчет об операциях банка Казань:");
		System.out.println();
		for (int i = 0; i < Bank.getTransArrLength(); i++) {
			int temp = i+1;
			System.out.println("Операция №"+temp);
			System.out.println(Bank.transactions[i].print());
			System.out.println();
		}
	}

	//печать отчета об операциях для определенного филиала
	public static void printBranchReport(int branch) {
		int temp = branch+1;
		System.out.println("Отчет об операциях филиала №"+temp);
		System.out.println("Адрес филиала: " + Bank.branches[branch].getAddress());
		System.out.println("Часы работы филиала: " + Bank.branches[branch].getAddress());
		System.out.println();
		for (int i = 0; i < Bank.branches[branch].getLengthTransactions(); i++) {
			temp = i+1;
			System.out.println("Операция №"+temp);
			System.out.println(Bank.branches[branch].transactions[i].print());
			System.out.println();
		}
	}

	//печать отчета об операциях для аккаунта
	public static void printAccountReport(Account _account) {
		System.out.println("Отчет об аккаунте " + Integer.toString(_account.getAccountID()+1) + " из филиала №" + Integer.toString(_account.getBranch()+1));
		System.out.println("Владелец карты: " + _account.getUserName() + " " + _account.getUserSurname());
		System.out.println();
		for (int i = 0; i < Bank.branches[_account.getBranch()].accounts[_account.getAccountID()]
				.getLengthOfTransactions(); i++) {
			int temp = i+1;
			System.out.println("Операция №"+temp);
			System.out.println(
					Bank.branches[_account.getBranch()].accounts[_account.getAccountID()].transactions[i].print());
			System.out.println();
		}
	}

	public static void main(String[] args) {
		// регистрация филиалов
		Bank.branches[0] = new Branch("Петров", "Казань, Уличная 1", "10-19", "2567894");
		Bank.branches[1] = new Branch("Петров", "Казань, Уличная 2", "10-19", "2567895");
		Bank.branches[2] = new Branch("Петров", "Казань, Уличная 3", "10-19", "2567896");
		Bank.branches[3] = new Branch("Петров", "Казань, Уличная 4", "10-19", "2567897");
		Bank.branches[4] = new Branch("Петров", "Казань, Уличная 5", "10-19", "2567898");

		// регистрация пользователей в филиалах
		Bank.branches[0].registerNewAccount("Виталий", "Городницкий", "12/06/1945", "122344", 1, 50000, 0);
		Bank.branches[0].registerNewAccount("Александра", "Пуповина", "12/06/1946", "122345", 2, 3000, 0);
		Bank.branches[0].registerNewAccount("Станислав", "Думов", "15/08/1939", "122378", 1, 57000, 0);
		Bank.branches[1].registerNewAccount("Стромае", "Папауте", "15/01/1992", "122311", 3, 5000000, 1);
		
		//проведение операций
		Bank.branches[0].accounts[0].deposit(200);
		Bank.branches[0].accounts[0].withdraw(200000);
		Bank.branches[0].accounts[0].transfer(1, 300);
		Bank.branches[0].accounts[1].withdraw(2400);

		//проведение кроссфилиальных транзакций
		Bank.crossBranchTransfer(0, 2, 1, 0, 7000);
		Bank.crossBranchTransfer(0, 2, 1, 0, 60000);

		//вызов функций окончания дня и месяца
		Bank.dayEnd();
		
		//печать отчетов
		printBankReport();
		printBranchReport(0);
		printBranchReport(1);
		printAccountReport(Bank.branches[0].accounts[0]);
		printAccountReport(Bank.branches[1].accounts[0]);
		
		//окончаниеМесяца
		Bank.mounthEnd();
		printBankReport();
	}
}