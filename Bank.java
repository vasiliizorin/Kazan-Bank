
public class Bank {
	private static int date = 1; // текущий операционный день
	private static double interestRateOrdinary = 0.05; // процентная ставка
														// обычная
	private static double interestRateBusiness = 0.01;// процентная ставка
														// бизнесс
	
	private static int maxTransactions = 50000;
	// хранение текущего количества операций
	private static int transArrLength = 0;

	// массив хранения всех транзакций
	static public Transaction[] transactions = new Transaction[maxTransactions];

	// массив для хранения всех филиалов банка
	static Branch[] branches = new Branch[5];

	// получение количества проведенных транзакций
	public static int getTransArrLength() {
		return transArrLength;
	}

	public static int getDate() {
		return date;
	}

	// регистрация новой транзакции
	public static boolean registrationTransaction(Transaction _newTransaction) {
		if (transArrLength < 5000) {
			transactions[transArrLength] = _newTransaction;
			transArrLength++;
			return true;
		} else {
			return false;
		}
	}

	// Перевод средств из одного филиала в другой
	public static boolean crossBranchTransfer(int senderBranch, int senderAccount, int receicerBranch,
			int receiverAccount, double amount) {
		Transaction newTransaction;
		if (amount > branches[senderBranch].accounts[senderAccount].getBalance() || amount <= 0) {
			return false;
		} else {
			if (branches[receicerBranch].accounts[receiverAccount].getStatus()) {
				branches[senderBranch].accounts[senderAccount].withdraw(amount);
				branches[receicerBranch].accounts[receiverAccount].deposit(amount);
			} else {
				newTransaction = new Transaction(Transaction.Operations.INERBRANCH, Integer.toString(Bank.getDate()),
						Integer.toString(senderAccount+1), Integer.toString(receiverAccount+1), false, amount, "",Integer.toString(senderBranch+1),Integer.toString(receicerBranch+1));
				registrationTransaction(newTransaction);
				return false;
			}
			newTransaction = new Transaction(Transaction.Operations.INERBRANCH, Integer.toString(Bank.getDate()),
					Integer.toString(senderAccount+1), Integer.toString(receiverAccount+1), true, amount, "",Integer.toString(senderBranch+1),Integer.toString(receicerBranch+1));
			registrationTransaction(newTransaction);
			return true;
		}
	}

	// завершение дня (начисление процентов)
	public static void dayEnd() {
		for (int i = 0; i < branches.length; i++) {
			for (int j = 0; j < branches[i].getLengthAccounts(); j++) {
				switch (branches[i].accounts[j].getType()) {
				case 1: {
					branches[i].accounts[j].interestPayment(interestRateOrdinary);
					break;
				}
				case 3: {
					branches[i].accounts[j].interestPayment(interestRateBusiness);
					break;
				}
				}
			}
		}
		if (date != 30) {
			date++;
		} else {
			mounthEnd();
			date = 1;
		}
	}

	// завершение меясяца
	public static void mounthEnd() {
		Bank.transArrLength = 0;
		Bank.transactions = new Transaction[maxTransactions];
		for (int i = 0; i < branches.length; i++) {
			branches[i].lengthTransactions = 0;
			branches[i].transactions = new Transaction[maxTransactions/5];
			for (int j = 0; j < branches[i].getLengthAccounts(); j++) {
				branches[i].accounts[j].transactions = new Transaction[maxTransactions/100];
				branches[i].accounts[j].lengthOfTransactions = 0;
				switch (branches[i].accounts[j].getType()) {
				case 2: {
					if (!branches[i].accounts[j].withdraw(1000)){
						branches[i].accounts[j].blockAccount();
					}
					break;
				}
				}
			}
		}
	}
}
