public class Branch {
	// хранение всех аккаунтов филиала
	public Account[] accounts = new Account[maxAmountOfAccounts];

	private static int maxAmountOfAccounts = 100;
	private static int maxAmountOfTransactions = 10000;

	// информация о филиале
	private String manager;
	private String address;
	private String businessHours;
	private String telephone;

	// конструктор филиала
	Branch(String _manager, String _address, String _businessHours, String _telephone) {
		manager = _manager;
		address = _address;
		businessHours = _businessHours;
		telephone = _telephone;
		lengthAccounts = 0;
		lengthTransactions = 0;
	}

	// хранение всех аккаунтов филиала
	private int lengthAccounts=0;

	// получение количества аккаунтов
	public int getLengthAccounts() {
		return lengthAccounts;
	}

	// Регистрация нового аккаунта
	public boolean registerNewAccount(String _name, String _surname, String _bithday, String _passportNumber,
			int newType, double startBalance, int _branch) {
		if (lengthAccounts < maxAmountOfAccounts) {
			// создание нового аккаунта
			accounts[lengthAccounts] = new Account(_name, _surname, _bithday, _passportNumber, newType, startBalance,
					_branch);
			lengthAccounts++;
			return true;
		} else {
			return false;
		}
	}

	// хранение всех операций внутри филиала
	public Transaction[] transactions = new Transaction[maxAmountOfTransactions];

	// количество проведенных транзакций
	public int lengthTransactions;

	// получение количества проведенных транзакций
	public int getLengthTransactions() {
		return lengthTransactions;
	}

	// регистрация новой транзакции
	public boolean registrationTransaction(Transaction _newTransaction) {
		if (lengthTransactions < maxAmountOfTransactions) {
			transactions[lengthTransactions] = _newTransaction;
			lengthTransactions++;
			return true;
		} else {
			return false;
		}
	}

	// получение информации о менеджере филиала
	public String getManager() {
		return manager;
	}

	// получение информации об адресе
	public String getAddress() {
		return address;
	}

	// получение информации о часах работы
	public String getBusinessHours() {
		return businessHours;
	}

	// получение информации о номере телефона
	public String getTelephone() {
		return telephone;
	}
}
