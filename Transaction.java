
public class Transaction {
	public static enum Operations {
		TRANSFER, DEPOSIT, WITHDRAW, LOCKING, UNLOCKING, INTEREST, INERBRANCH, TELLER
	}

	private Operations type; // тип операции
	private String date; // дата операции
	private String sender; // аккаунт - отправитель
	private String senderBranch; // филиал - отправитель
	private String reciever; // аккаунт - получатель
	private String recieverBranch; // филиал - получатель
	private boolean status; // статус операции
	private double amount; // сумма
	private String comment; // коментарий

	//регистрация новой транзакции
	Transaction(Operations _operationType, String _date, String _accountSender, String _accountReceiver,
			boolean _status, double _amount, String _comment, String _senderBranch,String _receiverBranch) {
		type = _operationType;
		date = _date;
		sender = _accountSender;
		reciever = _accountReceiver;
		status = _status;
		amount = _amount;
		senderBranch = _senderBranch;
		recieverBranch = _receiverBranch;
		if (_comment != "" && _comment != null) {
			comment = _comment;
		}
	}

	// получение типа транзакции
	public Operations getType() {
		return type;
	}

	// получение даты проведения транзакции
	public String getDate() {
		return date;
	}

	// получение номера отправителя
	public String getSender() {
		return sender;
	}

	// получение номера филиала - отправителя
	public String getSenderBranch() {
		return senderBranch;
	}

	// получение номера получателя
	public String getReceiver() {
		return reciever;
	}

	// получение номера филиала - получателя
	public String getReceiverBranch() {
		return recieverBranch;
	}

	// получение статуса транзакции
	public boolean getStatus() {
		return status;
	}

	// получение суммы транзакции
	public double getAmount() {
		return amount;
	}

	// получение комментария к транзакции
	public String getComment() {
		return comment;
	}
	// функция для вывода информации о транзакции
	public String print() {
		String result = "";
		switch (type) {
		case TRANSFER: {
			result = "Тип операции: перевод средств" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт-отправитель: " + sender + "\n" + "Аккаунт - получатель: "
					+ reciever + "\n" + "Сумма операции: " + amount + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case DEPOSIT: {
			result = "Тип операции: пополнение счета" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт: " + sender + "\n" + "Сумма операции: " + amount + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case WITHDRAW: {
			result = "Тип операции: снятие средств" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт: " + sender + "\n" + "Сумма операции: " + amount + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case TELLER: {
			result = "Тип операции: смена типа аккаунта" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт: " + sender + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case LOCKING: {
			result = "Тип операции: блокировка аккаунта" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт: " + sender + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case UNLOCKING: {
			result = "Тип операции: разблокировка аккаунта" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт: " + sender + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case INTEREST: {
			result = "Тип операции: начисление процента" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал: "
					+ senderBranch + "\n" + "Аккаунт: " + sender + "\n" + "Начисленный процент: " + amount + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		case INERBRANCH: {
			result = "Тип операции: межфилиальный перевод" + "\n" + "Дата проведения операции: " + date + "\n" + "Филиал отправитель: "
					+ senderBranch + "\n" + "Аккаунт-отправитель: " + sender + "\n" + "Филиал-получатель: "
							+ recieverBranch + "\n" + "Аккаунт-получатель: " + reciever + "\n" + "Сумма операции: " + amount + "\n";
			if (status) {
				result += "Статус операции: одобрена";
			} else {
				result += "Статус операции: отконена";
			}
			break;
		}
		}
		return result;
	}
}
